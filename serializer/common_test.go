package serializer_test

import (
	"gitee.com/anystreaming/moleculer-go"
	"gitee.com/anystreaming/moleculer-go/registry"
	. "github.com/moleculer-go/goemitter"

	log "github.com/sirupsen/logrus"
)

var logger = log.WithField("unit test pkg", "serializer_test")

func CreateLogger(name string, value string) *log.Entry {
	return logger.WithField(name, value)
}

func BrokerDelegates(nodeID string) *moleculer.BrokerDelegates {
	localBus := Construct()
	localNode := registry.CreateNode(nodeID, true, logger)
	broker := &moleculer.BrokerDelegates{
		LocalNode: func() moleculer.Node {
			return localNode
		},
		Logger: CreateLogger,
		Bus: func() *Emitter {
			return localBus
		}}
	return broker
}
