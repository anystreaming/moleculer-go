package broker_test

import (
	"fmt"
	"sync"
	"testing"
	"time"

	"gitee.com/anystreaming/moleculer-go"
	"gitee.com/anystreaming/moleculer-go/broker"
)

//var _ = Describe("Broker", func() {
//
//	It("Should make a local call and return results", func() {
//		actionResult := "abra cadabra"
//		service := moleculer.ServiceSchema{
//			Name: "do",
//			Actions: []moleculer.Action{
//				moleculer.Action{
//					Name: "stuff",
//					Handler: func(ctx moleculer.Context, params moleculer.Payload) interface{} {
//						return actionResult
//					},
//				},
//			},
//		}
//
//		broker := broker.New(&moleculer.Config{
//			LogLevel: "ERROR",
//		})
//		broker.Publish(service)
//		broker.Start()
//
//		result := <-broker.Call("do.stuff", 1)
//
//		fmt.Printf("Results from action: %s", result)
//
//		Expect(result.Value()).Should(Equal(actionResult))
//
//	})
//
//	It("Should make a local call, call should panic and returned paylod should contain the error", func() {
//		service := moleculer.ServiceSchema{
//			Name: "do",
//			Actions: []moleculer.Action{
//				moleculer.Action{
//					Name: "panic",
//					Handler: func(ctx moleculer.Context, params moleculer.Payload) interface{} {
//						if params.Bool() {
//							panic(errors.New("some random error..."))
//						}
//						return "no panic"
//					},
//				},
//			},
//		}
//		mem := &memory.SharedMemory{}
//		baseConfig := &moleculer.Config{
//			LogLevel: "error",
//			TransporterFactory: func() interface{} {
//				transport := memory.Create(log.WithField("transport", "memory"), mem)
//				return &transport
//			},
//		}
//		bkrConfig := &moleculer.Config{
//			DiscoverNodeID: func() string { return "do-broker" },
//		}
//		bkr := broker.New(baseConfig, bkrConfig)
//		bkr.Publish(service)
//		bkr.Start()
//
//		result := <-bkr.Call("do.panic", true)
//
//		Expect(result.IsError()).Should(Equal(true))
//		Expect(result.Error().Error()).Should(BeEquivalentTo("some random error..."))
//
//		service = moleculer.ServiceSchema{
//			Name:         "remote",
//			Dependencies: []string{"do"},
//			Actions: []moleculer.Action{
//				moleculer.Action{
//					Name: "panic",
//					Handler: func(ctx moleculer.Context, params moleculer.Payload) interface{} {
//						result := <-ctx.Call("do.panic", params)
//						ctx.Logger().Debug("params: ", params, " result: ", result.Value())
//						if result.IsError() {
//							panic(result.Error())
//						}
//						return result
//					},
//				},
//			},
//		}
//		bkrConfig = &moleculer.Config{
//			DiscoverNodeID: func() string { return "remote-broker" },
//		}
//		bkrRemote := broker.New(baseConfig, bkrConfig)
//		bkrRemote.Publish(service)
//		bkrRemote.Start()
//
//		bkrRemote.WaitFor("do")
//		result = <-bkrRemote.Call("remote.panic", true)
//
//		Expect(result.IsError()).Should(Equal(true))
//		Expect(result.Error().Error()).Should(BeEquivalentTo("some random error..."))
//
//		bkr.WaitFor("remote")
//		result = <-bkr.Call("remote.panic", false)
//
//		Expect(result.IsError()).Should(Equal(false))
//		Expect(result.String()).Should(BeEquivalentTo("no panic"))
//
//		bkrRemote.Stop()
//		bkr.Stop()
//	})
//
//	It("Should call multiple local calls (in chain)", func() {
//
//		actionResult := "step 1 done ! -> step 2: step 2 done ! -> magic: Just magic !!!"
//		service := moleculer.ServiceSchema{
//			Name: "machine",
//			Actions: []moleculer.Action{
//				moleculer.Action{
//					Name: "step1",
//					Handler: func(ctx moleculer.Context, params moleculer.Payload) interface{} {
//						step2Result := <-ctx.Call("machine.step2", 0)
//						return fmt.Sprintf("step 1 done ! -> step 2: %s", step2Result.String())
//					},
//				},
//				moleculer.Action{
//					Name: "step2",
//					Handler: func(ctx moleculer.Context, params moleculer.Payload) interface{} {
//						magicResult := <-ctx.Call("machine.magic", 0)
//						return fmt.Sprintf("step 2 done ! -> magic: %s", magicResult.String())
//					},
//				},
//				moleculer.Action{
//					Name: "magic",
//					Handler: func(ctx moleculer.Context, params moleculer.Payload) interface{} {
//						ctx.Emit("magic.happened, params", "Always !")
//						return "Just magic !!!"
//					},
//				},
//			},
//		}
//
//		broker := broker.New(&moleculer.Config{
//			LogLevel: "ERROR",
//		})
//		broker.Publish(service)
//		broker.Start()
//
//		result := <-broker.Call("machine.step1", 1)
//
//		fmt.Printf("Results from action: %s", result)
//
//		Expect(result.Value()).Should(Equal(actionResult))
//	})
//
//	It("Should make a local call and return results (service schema as pointer)", func() {
//		actionResult := "abra cadabra"
//		service := &moleculer.ServiceSchema{
//			Name: "do",
//			Actions: []moleculer.Action{
//				moleculer.Action{
//					Name: "stuff",
//					Handler: func(ctx moleculer.Context, params moleculer.Payload) interface{} {
//						return actionResult
//					},
//				},
//			},
//		}
//
//		broker := broker.New(&moleculer.Config{
//			LogLevel: "ERROR",
//		})
//		broker.Publish(service)
//		broker.Start()
//
//		result := <-broker.Call("do.stuff", 1)
//
//		fmt.Printf("Results from action: %s", result)
//
//		Expect(result.Value()).Should(Equal(actionResult))
//
//	})
//})

var mathService = moleculer.ServiceSchema{
	Name: "goMath",
	Actions: []moleculer.Action{
		{
			Name: "add",
			Handler: func(ctx moleculer.Context, params moleculer.Payload) interface{} {
				return params.Get("a").Int() + params.Get("b").Int()
			},
		},
	},
	Events: []moleculer.Event{
		{
			Name: "goEvents",
			Handler: func(context moleculer.Context, params moleculer.Payload) {
				fmt.Println("js call go events")
			},
		},
	},
}

func TestBrokerCreate(t *testing.T) {
	var bkr = broker.New(&moleculer.Config{
		DiscoverNodeID: func() string {
			return "test_go1"
		},
		LogLevel:    "trace",
		Transporter: "redis://",
		RedisConfig: &moleculer.RedisConfig{
			Addr:     "192.168.1.20:6379",
			PassWord: "wangshikeji2019",
			DB:       0,
		},
	})
	bkr.Publish(mathService)
	bkr.Start()

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		for {
			time.Sleep(3 * time.Second)
			fmt.Println("go call js")
			ch := bkr.Call("v1.task.goCall", map[string]interface{}{
				"a": 10,
				"b": 20,
			})
			res := <-ch
			fmt.Println(res)
		}
	}()
	wg.Wait()
	bkr.Stop()
}
